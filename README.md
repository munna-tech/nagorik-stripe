<p align="center">
    <a href="https://nagorik.tech" target="_blank">
        <img src="https://cdn-djbkn.nitrocdn.com/kMWeKIzdkHYqpOLZxIXqmDzDtlpNawjC/assets/static/optimized/rev-64216cb/wp-content/uploads/2021/10/nagorik-logo1-2048x565.png" width="400" alt="Nagorik Tech Logo">
    </a>
</p>

<!-- <p align="center">
    <a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
    <a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
    <a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
    <a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p> -->

## About Nagorik Stripe

Welcome to the Nagorik Stripe Package! This Composer package is designed to streamline the integration of Stripe payment services into your Laravel applications. Whether you're building an e-commerce platform, a subscription-based service, or simply need to handle payments, this package aims to make the process smooth and developer-friendly.

- Create payment url.
- Create subscription.
- Create price.
- Create invoice.
- Creaet product.
- Subscription webhook management.
- Payment validation.
- Fully managed dashboard.
- Manager user subscriptions.
- Cancel Subscription.
- Sent subscription invoice.
- Download invoice & receipt
- And more...

## Install Nagorik/Stripe

Next, run the Composer command to install the latest stable version:

```bash
composer require nagorik/stripe
```

Publish the configuration files:

```bash
php artisan vendor:publish --tag=ngstripe
```

If you update the resources/assets forcefully 

```bash
php artisan vendor:publish --tag=ngstripe --force
```

## Setup configuration

You have to must change your configuration file:
Path: config/ngstripe.php

```php
<?php
    return [
        /**
        * Set pacakge base url
        * Example: https://example.com
        * Your hosted url where you want to use this package
        */
        "base_url" => env("BASE_URL", "https://22c1-103-203-93-214.ngrok-free.app"),

        /**
        * Set app icon
        * Example: /ngstripe/icons/favicon.png
        * Favicon path
        */
        "app_icon" => "/ngstripe/icons/favicon.png",
        "fav_icon" => "/ngstripe/icons/favicon.png",

        /**
        * Assets/resource path will use https if enable
        * Redirection will use https if enable
        */
        "https" => true,

        /**
        * Set payment information
        * api_key: Stripe api secret key [secret_key]
        * redirect_url: After payment success where to redirect default is null
        * go_back_url: After payment where to redirect by click on back button on payment success/failed page. default is /
        * txn_prefix: Its very important. If you have multiple project on same stripe account then you must set this. Its should be unique for each project.
        * subscription_holding_days: After subscription expired, how many days you want to hold the subscription. default is 1 day
        */

        "payment" => [
            "api_key" => "sk_test_51MEW0yLRN9lOZ6qkcEBkuprYMsI51MEW0yLRN9lOZ6qkcEBkuprYMsI51MEW0yLRN9lOZ6qkcEBkuprYMsI",
            "redirect_url" => null,
            "go_back_url" => "/",
            "txn_prefix" => "ns_chobi_ai",
            "subscription_holding_days" => 1
        ],

        /**
        * Notificaiton settings
        * class: Class name with namespace
        * method: Method name
        * This method will call after payment success/failed
        */
        "notify" => [
            'class' => 'App\\Http\\Controllers\\StripeController',
            'method' => 'receivedPayment'
        ],

        /**
        * Webhook settings
        * class: Class name with namespace
        * method: Method name
        * This method will call after payment success/failed
        */
        "webhook" => [
            'class' => 'App\\Http\\Controllers\\StripeController',
            'method' => 'receivedWebhook'
        ],

        /**
        * Package settings
        * Info: Its your own local database table. Where are you storing your package information.
        * table: Table name
        * primary_key: Primary key of the table like id.
        * Uses: This package will use this table to get package information and store payment information and manage subscription.
        * Its very important to set this table and primary key.
        */
        'package' => [
            'table' => 'packages',
            'primary_key' => 'id',
        ],

        /**
         * Message settings
         * You can change the messages here
         */
        "message" => [
            "payment" => [
                "success" => [
                    "title" => "Payment Done!",
                    "message" => "Your payment has been successfully completed.",
                ],
                "failed" => [
                    "title" => "Payment Failed!",
                    "message" => "Your payment has been failed.",
                ],
            ],
            "subscription" => [
                "success" => [
                    "title" => "Subscription Done!",
                    "message" => "Your subscription has been successfully completed.",
                ],
                "failed" => [
                    "title" => "Subscription Failed!",
                    "message" => "Your subscription has been failed.",
                ],
            ],
        ]
    ];

```

## Install

To install and configuration the package into your laravel applciation:

```bash
php artisan ngstripe:install
```

## Usage

To use this package use this namespace on your any php file

```php
use Nagorik\Stripe\NgStripe;
```

### Create payment url:

```php
// create instance of the ngstripe class
$ngStripe = new NgStripe();

// provide the datas
$data = [
    "package_id" => $id, // set package id from your package table
    "price" => 10, // set checkout price
    "user_id" => 1, // user id
    "currency" => "usd", // default usd [must use which currency is supported by stripe]
    "user_email" => 'user@gmail.comd', // user email
    "discount" => 10, // discount if need otherwise comment it
    "discount_type" => "flat", // discount type [flat, percentage] if need
    "coupon" => "test-coupon", // if you have set any coupon system on your system
    "redirect" => false, // if its enable, then you will get the object of your payment data otherwise you will get only payment url
];

// make payment url
$url = $ngStripe->createPayment($data);

// if redirect = false set
// $data = $data->data;
// $url = $data->url;
// return redirect($url)

// finallay return the url
return $url;
```

### Create subscription url:

```php
// create instance of the ngstripe class
$ngStripe = new NgStripe();

// provide the datas
$data = [
    "package_id" => $id, // set package id from your package table
    "price" => 10, // set checkout price
    "user_id" => 1, // user id
    "currency" => "usd", // default usd [must use which currency is supported by stripe]
    "user_email" => 'user@gmail.comd', // user email
    "discount" => 10, // discount if need otherwise comment it
    "discount_type" => "flat", // discount type [flat, percentage] if need
    "recurring" => "day", // must use [day, month, year]
    "recurring_period" => 1, // must use reccurning period
    "coupon" => "test-coupon", // if you have set any coupon system on your system
    "redirect" => false, // if its enable, then you will get the object of your payment data otherwise you will get only payment url
];

// make payment url
$url = $ngStripe->createSubscription($data);

// if redirect = false set
// $data = $data->data;
// $url = $data->url;
// return redirect($url)

// finallay return the url
return $url;
```

### After payment/subscription you will get notify:

```php
// If you have already set the notify array block on your config/ngstripe.php
// This notify will return from ngstripe pacakge

// you will received
$data = [
   "status" => "paid", // payment status
   "type" => "payment", // payment type [payment, subscription]
   "user_id" => "1", // payment user id [your base table User table id]
   "package_id" => "1", // payment pacakge id [Your package table id]
   "txn_id" => "ns_chobi_ai-3iYOf7HlyF", // payment txn ID
   "pay_id" => "cs_test_a1O2K4OQO98FxvqdO2EcVRXpdcbJV19XXbeW2vVkbdAMkZ6az5RtNCJTjy" // payment pay id from stripe
];

```

### After payment/subscription you will get webhook:

```php
// If you have already set the webhook array block on your config/ngstripe.php
// This webhook will return from the stripe dashboard

// you will received data
$data = [
  "invoiceId" => "in_1OJrEvLRN9lOZ6qkn30UqFMP",
  "price_id" => "price_1OJrEmLRN9lOZ6qkzMNGS4GQ",
  "charge" => "ch_3OJrEvLRN9lOZ6qk0rytDcVp",
  "amount" => 9,
  "txn_id" => "ns_chobi_ai-g0i3OJsaws",
  "app_prefix" => "ns_chobi_ai",
  "subscription" => "sub_1OJrEvLRN9lOZ6qkC89vTMSi",
  "invoiceUrl" => "https://invoice.stripe.com/i/acct_1MEW0yLRN9lOZ6qk/test_YWNjdF8xTUVXMHlMUk45bE9aNnFrLF9QODdUWUhXMFp1RnlRbGE0NnYyYkVhQ3JDVEtZSXV4LDkyMjk1MTM30200YeTEifzV?s=ap"
]; // you will get and json endcode string from stripe

```

## Methods

### Get user subscription

```php

use Nagorik\Stripe\NgStripe;
$ngStripe = new NgStripe();

$userId = 1; // set your user id
$subscriptions = $ngStripe->userSubscription($id);

// its return the all subscriptions as an array

```

### Get user indivitual package subscription

```php

use Nagorik\Stripe\NgStripe;
$ngStripe = new NgStripe();

$userId = 1; // set your user id
$packId = 1; // set your package id
$subscriptionPackage = $ngStripe->userPackageSubscription($id, $packId);

// it will return the the subscription if found or null

```

### Check user has subscribed or not

```php

use Nagorik\Stripe\NgStripe;
$ngStripe = new NgStripe();

$userId = 1; // set your user id
$packId = 1; // set your package id [Optional]
$isSubscribed = $ngStripe->isSubscriber($id, $packId = null);

// it will return boolean value [true, false]

```

### Check users all transactions

```php

use Nagorik\Stripe\NgStripe;
$ngStripe = new NgStripe();

$userId = 1; // set your user id
$packId = 1; // set your package id [Optional]
$txns = $ngStripe->userTransactions($id, $packId = null);

// it will return transactions list of an user

```

### Check users all Payment Transactions

```php

use Nagorik\Stripe\NgStripe;
$ngStripe = new NgStripe();

$userId = 1; // set your user id
$packId = 1; // set your package id [Optional]
$paymentTxns = $ngStripe->userPaymentTransactions($id, $packId = null);

// it will return payment transactions list of an user

```

### Check users all Subscription Transactions

```php

use Nagorik\Stripe\NgStripe;
$ngStripe = new NgStripe();

$userId = 1; // set your user id
$packId = 1; // set your package id [Optional]
$subscriptionTxns = $ngStripe->userSubscriptionTransactions($id, $packId = null);

// it will return subscription transactions list of an user

```

### Check users all User Invoices

```php

use Nagorik\Stripe\NgStripe;
$ngStripe = new NgStripe();

$userId = 1; // set your user id
$packId = 1; // set your package id [Optional]
$invoices = $ngStripe->userInvoices($id, $packId = null);

// it will return invoices list of an user

```

### Get transaction lists

```php

use Nagorik\Stripe\NgStripe;
$ngStripe = new NgStripe();

$status = null; // Optional [Status: 1, 2, 0] default: null
$paginate = true; // Optional [Paginate: true, false] default: true
$perPage = 10; // Optional [Perpage: 10] default: 10

$transaction = $ngStripe->txnLists($status, $paginate, $perPage);

// transaction lists

```

### Get transaction details

```php

use Nagorik\Stripe\NgStripe;
$ngStripe = new NgStripe();

$txn_id = "txn_id"; // your txn id
$transaction = $ngStripe->getTxn($txn_id);

// it will return transaction details

```

### Get transactions by coupon

```php

use Nagorik\Stripe\NgStripe;
$ngStripe = new NgStripe();

$coupon = "test-coupon"; // your txn id
$userId = 1; // [optional] if want to filter against any user id but its optional
$transaction = $ngStripe->getTxnByCoupon($coupon, $userId);

// it will return transaction lists

```

### Download txn invoice

```php

use Nagorik\Stripe\NgStripe;
$ngStripe = new NgStripe();

$txnId = "txn_id"; // your txn id
$transaction = $ngStripe->downloadInvoice($txnId);

// it will download the invoice

```

### Get subscription details

```php

use Nagorik\Stripe\NgStripe;
$ngStripe = new NgStripe();

$subscription_id = "subscription_id"; // your subscription id
$subscription = $ngStripe->getSubscriptionDetails($subscription_id);

// it will return subscription details

```


### Get invoice details

```php

use Nagorik\Stripe\NgStripe;
$ngStripe = new NgStripe();

$invoice_id = "invoice_id"; // your invoice id
$invoice = $ngStripe->getInvoice($invoice_id);

// it will return invoice pdf url

```

## Subscription checkup

You can use this command to checkup user's subscription validity

```php
php artisan ngstripe:check-subscription
```

You can set this command in your app/Console/karnel.php file and run a "php artisan schedule:run" via cron or supervisor into your server.

Example bellow

```php
    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->command('ngstripe:check-subscription')->dailyAt("00:00");
    }
```

## Cancel Subscription

To cancel the users all subscriptions

```php

use Nagorik\Stripe\NgStripe;
$ngStripe = new NgStripe();

$userId = 1; // set your user id
$subscriptionTxns = $ngStripe->cancelAllSubscription($userId);

// it will return 
$data = [
  "total_subscription" => 0,
  "total_cancelled" => 0,
  "total_cancelled_failed" => 0
];

```

To cancel the users subscriptions by subscription id

```php

use Nagorik\Stripe\NgStripe;
$ngStripe = new NgStripe();

$userId = 1; // set your user id
$subscriptionId = 1; // set your user subscription id
$subscriptionTxns = $ngStripe->cancelSubscription($userId, $subscriptionId);

// it will return boolean value [true, false]

```

To cancel all subscriptions of the single package

```php

use Nagorik\Stripe\NgStripe;
$ngStripe = new NgStripe();

$userId = 1; // set your user id
$packageId = 1; // set your package id
$subscriptionTxns = $ngStripe->cancelPackageSubscription($userId, $packageId);

// it will return 
$data = [
  "total_subscription" => 0,
  "total_cancelled" => 0,
  "total_cancelled_failed" => 0
];

```

## Uninstall

To uninstall the package from your laravel applciation:

```bash
php artisan ngstripe:uninstall
```

Finally remove from composer.json:

```bash
composer remove nagorik/stripe
```

## Security Vulnerabilities

If you discover a security vulnerability within this pacakge, please send an e-mail to Munna Ahmed via [munna@nagorik.tech](mailto:munna@nagorik.tech). All security vulnerabilities will be promptly addressed.

## License

The Nagorik Stripe Package is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
