<?php

namespace Nagorik\Stripe;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Redirect;
use Nagorik\Stripe\Services\NgStripeService;
use Nagorik\Stripe\Exception\NgStripeException;
use Nagorik\Stripe\Services\NgUserSubscriptionService;

class NgStripe
{

    protected $service, $ngUserSubscription;

    public function __construct()
    {
        $this->checkConfiguration();
        $this->service = new NgStripeService();
        $this->ngUserSubscription = new NgUserSubscriptionService();
    }

    // require package id
    public function createPayment($data) {
        $productResponse = $this->service->createPayment($data);
        $responseData = $productResponse->data();

        if ($responseData->status == false) {
            return $responseData;
        }

        $redirect = $data['redirect'] ?? true;
        if ($redirect) {
            $url = $responseData->data->url;
            return redirect($url);
        }
        return $responseData;
    }

    // require package id
    public function createSubscription($data) {
        $productResponse = $this->service->createSubscription($data);
        $responseData = $productResponse->data();

        if ($responseData->status == false) {
            return $responseData;
        }

        $redirect = $data['redirect'] ?? true;
        if ($redirect) {
            $url = $responseData->data->url;
            return redirect($url);
        }
        return $responseData;
    }

    public function getPayment($id) {
        $productResponse = $this->service->getPayment($id);
        $responseData = $productResponse->data();

        if ($responseData->status == false) {
            return $responseData;
        }

        return $responseData;
    }

    public function checkConfiguration() {
        // check package table
        $table = config("ngstripe.package.table");

        // check payment table is available or not
        if (!Schema::hasTable($table)) {
            throw new NgStripeException("Table not found", "Package data table not found, set your package table name correctly in ngstripe config file", 400);
        }
    }

    public function jsonPath($path = null) {
        $getPath = __DIR__."/Json";
        if ($path) {
            $getPath = $getPath."/".$path;
        }
        return $getPath;
    }

    // user subscription
    public function userSubscription($userId) {
        return $this->ngUserSubscription->getSubscription($userId);
    }

    // user subscription for individual package
    public function userPackageSubscription($userId, $packId) {
        return $this->ngUserSubscription->userPackageSubscription($userId, $packId);
    }

    // user isSubscriber or individual package subscriber
    public function isSubscriber($userId, $packId = null) {
        return $this->ngUserSubscription->isSubscriber($userId, $packId);
    }

    // user isSubscriber or individual package subscriber
    public function userTransactions($userId, $packId = null) {
        return $this->ngUserSubscription->userTransactions($userId, $packId);
    }

    // user isSubscriber or individual package subscriber
    public function userInvoices($userId, $packId = null) {
        return $this->ngUserSubscription->userInvoices($userId, $packId);
    }

    // user isSubscriber or individual package subscriber
    public function userPaymentTransactions($userId, $packId = null) {
        return $this->ngUserSubscription->userPaymentTransactions($userId, $packId);
    }

    // user isSubscriber or individual package subscriber
    public function userSubscriptionTransactions($userId, $packId = null) {
        return $this->ngUserSubscription->userSubscriptionTransactions($userId, $packId);
    }

    // user isSubscriber or individual package subscriber
    public function cancelSubscription($userId, $subscription) {
        return $this->ngUserSubscription->cancelSubscription($userId, $subscription);
    }

    // user isSubscriber or individual package subscriber
    public function cancelPackageSubscription($userId, $packId) {
        return $this->ngUserSubscription->cancelPackageSubscription($userId, $packId);
    }

    // cancel user subscription all
    public function cancelAllSubscription($userId) {
        return $this->ngUserSubscription->cancelAllSubscription($userId);
    }

    // getTxn
    public function getTxn($txn_id) {
        return $this->ngUserSubscription->getTxn($txn_id);
    }

    // getSubscription
    public function getSubscription($subscription_id) {
        return $this->ngUserSubscription->getSubscriptionDetails($subscription_id);
    }

    // getInvoice
    public function getInvoice($invoice_id) {
        return $this->ngUserSubscription->getInvoice($invoice_id);
    }

    // get transaction lists
    public function getTxnByCoupon($coupon, $userId = null) {
        return $this->ngUserSubscription->getTxnByCoupon($coupon, $userId);
    }

    // get transaction lists
    public function downloadInvoice($txnId) {
        return $this->ngUserSubscription->downloadInvoice($txnId);
    }

    // get transaction lists
    public function txnLists($status = null, $paginate = true, $perPage = 10) {
        return $this->ngUserSubscription->txnLists($status, $paginate, $perPage);
    }
}
