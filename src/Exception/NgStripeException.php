<?php

namespace Nagorik\Stripe\Exception;

use Exception;

class NgStripeException extends Exception
{
    protected $title = null;
    protected $message = "NgStripe Exception";
    protected $code = 0;
    protected $array = [];


    public function __construct($title = "", $message = "", $code = 400, $array = [])
    {
        $this->title = $title;
        $this->message = $message;
        $this->code = $code;
        $this->array = $array;
    }

    // render
    public function render() {
        $data = array_merge([
            "status" => false,
            "title" => $this->title,
            "message" => $this->message,
        ], $this->array);
        return response()->json($data, $this->code);
    }
}
