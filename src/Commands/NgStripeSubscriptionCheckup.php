<?php

namespace Nagorik\Stripe\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Nagorik\Stripe\Models\NSUserSubscription;

class NgStripeSubscriptionCheckup extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ngstripe:check-subscription';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check the users subscription status and update';

    /**
     * Execute the console command.
     */
    public function handle() {

        $total = 0;
        $totalCancelled = 0;

        $line = "--------------------------------------------------------------------------------------------------------";
        $this->info($line);
        $this->info("NgStripe Subscription Checkup");
        $this->info($line);

        $subscriptions = NSUserSubscription::where("is_active", 1)->get();

        foreach ($subscriptions as $subscription)
        {
            $endsAt = $subscription->ends_at;


            if ($endsAt < Carbon::now()) {
                $total++;
            }

            $now = Carbon::now();
            $addDay = config("ngstripe.payment.subscription_holding_days");
            $waiting = $now->addDays($addDay);

            if ($endsAt < $waiting)
            {
                // subscription expired
                $subscription->is_active = 0;
                $subscription->status = 0;
                $subscription->updated_at = $now;
                $subscription->save();

                $totalCancelled++;
            }
        }

        $this->info("Total ({$total}) subscription's has been expired");
        $this->info($line);

        $this->info("Total ({$totalCancelled}) subscription's has been cancelled");
        $this->info($line);
    }

}
