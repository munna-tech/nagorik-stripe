<?php

namespace Nagorik\Stripe\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Nagorik\Stripe\Services\NgStripeWebhookService;

class NgStripeInstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ngstripe:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install the ngstripe payment system';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $line = "--------------------------------------------------------------------------------------------------------";
        $this->line($line);
        $this->info('** Installing the packages into the system');

        // publish the public assets
        $this->line($line);
        $this->line('-> Publishing the ngstripe public assets');
        $this->line($line);

        // call the artisan command
        // check already published or not
        // $assetFolder = base_path('config/ngstripe.php');
        // if(!File::exists($assetFolder)) {
        //     $this->call('vendor:publish', [
        //         '--provider' => "Nagorik\Stripe\NgStripeServiceProvider",
        //         '--tag' => "ngstripe",
        //         '--force' => true
        //     ]);
        //     $this->info('** published the ngstripe public assets');
        // }
        // else
        // {
        //     $this->info('** already published the ngstripe public assets');
        // }

        // migrate the databases
        // $this->line($line);

        $this->line('-> Migrating the ngstripe migrations into database');
        $this->line($line);
        // confirm the migration
        if ($this->confirm('Do you wish to continue (Required)?')) {
            $this->call('migrate');
            $this->info('** migrated the ngstripe database');
        }
        else
        {
            $this->info('** skipped the ngstripe database migration');
            $this->info('** You can run the migration command manually');
            $this->info('php artisan migrate');
        }

        // publish the public assets
        $this->line($line);
        $this->line('-> Creating the ngstripe webhooks');
        $this->line($line);

        $ngService = new NgStripeWebhookService();
        $alreadyExists = $ngService->checkWebHookExists();

        if ($alreadyExists) {
            $this->info("Webhook checkup is done and setup is ok");
        }
        else
        {
            $this->info("Webhook is configuring.");
            $this->line($line);

            $data = $ngService->createWebhooks();
            if ($data) {
                $this->info("-> Webhook is configured successfully");
            }
            else
            {
                $this->info("-> Webhook is not configured successfully");
            }
        }

        $this->line($line);
        $this->line('-> Finishig installtion');
        $this->line($line);
        $this->info('** Installed the packages into the system');
    }
}

