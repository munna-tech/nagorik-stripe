<?php

namespace Nagorik\Stripe\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Nagorik\Stripe\Services\NgStripeWebhookService;

class NgStripeUninstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ngstripe:uninstall';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removing the packages from the system';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $line = "--------------------------------------------------------------------------------------------------------";
        $this->line($line);
        $this->info('** Removing the packages from the system');

        // delete the public assets
        $this->line($line);
        $this->line('-> Deleting the ngstripe public assets');

        $assetFolder = public_path('ngstripe');
        if(File::exists($assetFolder)) {
            File::deleteDirectory($assetFolder);
            $this->line($line);
            $this->info('** The ngstripe public assets deleted successfully');
        } else {
            $this->line($line);
            $this->info('** Deleting the ngstripe public assets got error, may be permission issue or not found. delete manually');
        }

        // delete the views assets
        $this->line($line);
        $this->line('-> Deleting the ngstripe views assets');

        $assetFolder = resource_path('views/vendor/ngstripe');
        if(File::exists($assetFolder)) {
            File::deleteDirectory($assetFolder);
            $this->line($line);
            $this->info('** The ngstripe views assets deleted successfully');
        } else {
            $this->line($line);
            $this->info('** Deleting the ngstripe views assets got error, may be permission issue or not found. delete manually');
        }


        // delete the config files
        $this->line($line);
        $this->line('-> Deleting the ngstripe config');

        $assetFolder = base_path('config/ngstripe.php');
        if(File::exists($assetFolder)) {
            File::delete($assetFolder);
            $this->line($line);
            $this->info('** The ngstripe config deleted successfully');
        } else {
            $this->line($line);
            $this->info('** Deleting the ngstripe config assets got error, may be permission issue or not found. delete manually');
        }

        // delete databases from db
        $this->line($line);
        $this->line('-> Deleting the nagorik/stripe tables from database');
        if ($this->confirm('Do you wish to continue?')) {
            $tables = [
                'ngstripe_failed_transactions',
                'ngstripe_transactions',
                'ngstripe_user_subscriptions',
                'ngstripe_user_cancelled_subscriptions',
                'ngstripe_subscription_invoices',
                'ngstripe_user_activities'
            ];

            $migrations = [
                '2023_11_30_1001_ngstripe_failed_transactions',
                '2023_11_30_1002_ngstripe_transactions',
                '2023_11_30_1003_ngstripe_user_subscriptions',
                '2023_11_30_1004_ngstripe_subscription_invoices',
                '2023_11_30_1003_ngstripe_user_activity',
                '2023_11_30_1003_ngstripe_user_cancelled_subscriptions',
                '2023_12_24_1000_ngstripe_coupon_to_transactions'
            ];

            foreach ($tables as $item)
            {
                $this->line('-> Deleting the table: ' . $item);
                DB::statement('DROP TABLE IF EXISTS ' . $item);
                $this->line('-> Deleted the table: ' . $item);
            }
            $this->line($line);
            $this->info('** Deleting the nagorik/stripe tables from database finished successfully');

            // delete migrations from db
            $this->line($line);
            $this->line('-> Deleting the nagorik/stripe migrations from database');

            foreach ($migrations as $item)
            {
                $this->line('-> Deleting the migration: ' . $item);
                DB::statement('DELETE FROM migrations WHERE migrations.`migration` = "' . $item . '"');
                $this->line('-> Deleted the migration: ' . $item);
            }
            $this->line($line);
            $this->info('** Deleting the nagorik/stripe migrations from database finished successfully');
        }

        // delete the webhook files
        $this->line($line);
        $this->line('-> Deleting the webhook configuration');

        $ngService = new NgStripeWebhookService();
        $removeWebhook = $ngService->removeWebHook();

        if ($removeWebhook) {
            $this->info("-> Webhook is removed successfully");
        }
        else
        {
            $this->info("-> Webhook removed was failed remove it manually from stripe dashboard");
        }

        $this->line($line);
        $this->line('-> Finishing');
        $this->line($line);
        $this->info('** Finished. Thanks for using the Nagorik Stripe Package');

        $this->line($line);
        $this->line('-> Run the final command to remove the package from vendor');
        $this->line($line);
        $this->info('composer remove nagorik/stripe');
    }
}
