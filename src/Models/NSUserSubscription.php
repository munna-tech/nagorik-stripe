<?php

namespace Nagorik\Stripe\Models;

use Illuminate\Database\Eloquent\Model;

class NSUserSubscription extends Model
{
    protected $table = 'ngstripe_user_subscriptions';

}
