<?php

namespace Nagorik\Stripe\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class NSTransaction extends Model
{
    protected $table = 'ngstripe_transactions';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'recurring' => 'array',
        'data' => 'array',
    ];

    public function user() {
        return $this->belongsTo(User::class, "user_id");
    }

    public function getPrice() {
        return bcmul($this->price / 1, 1, 2);
    }

    public function getDiscount() {
        return bcmul($this->discount / 1, 1, 2);
    }

    public function getTotal() {
        return bcmul($this->total / 100, 1, 2);
    }

    public function lastInvoice() {
        $invoice = NSSubscriptionInvoice::where("st_id", $this->st_id)->orderBy("id", "desc")->first();
        return $invoice;
    }

}
