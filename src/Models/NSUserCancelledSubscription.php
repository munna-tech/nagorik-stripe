<?php

namespace Nagorik\Stripe\Models;

use Illuminate\Database\Eloquent\Model;

class NSUserCancelledSubscription extends Model
{
    protected $table = 'ngstripe_user_cancelled_subscriptions';

}
