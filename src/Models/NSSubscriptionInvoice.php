<?php

namespace Nagorik\Stripe\Models;

use Illuminate\Database\Eloquent\Model;

class NSSubscriptionInvoice extends Model
{
    protected $table = 'ngstripe_subscription_invoices';

}
