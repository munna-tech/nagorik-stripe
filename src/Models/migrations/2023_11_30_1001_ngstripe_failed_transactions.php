<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ngstripe_failed_transactions', function (Blueprint $table) {
            $table->id();
            $table->string('txn_id')->index()->unique();
            $table->unsignedBigInteger('package_id')->index();
            $table->unsignedBigInteger('user_id')->index();
            $table->string('prod_id')->nullable();
            $table->string('price_id')->nullable();
            $table->string('st_id')->nullable()->index();
            $table->double('price');
            $table->integer('is_subscription')->default(0)->index();
            $table->double('discount')->default(0);
            $table->double('total');
            $table->longText('pay_url')->nullable();
            $table->integer('pay_status')->default(0);
            $table->integer('pay_check')->default(0);
            $table->json('recurring')->nullable();
            $table->longText('subscription_id')->nullable();
            $table->longText('charge_id')->nullable();
            $table->json('data')->nullable();
            $table->longText('charge_data')->nullable();
            $table->integer('status')->default(0);
            $table->integer('is_expired')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ngstripe_failed_transactions');
    }
};
