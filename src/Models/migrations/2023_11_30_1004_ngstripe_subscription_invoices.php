<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ngstripe_subscription_invoices', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('package_id')->index();
            $table->longText('st_id')->nullable();
            $table->longText('invoice_id')->nullable();
            $table->longText('price_id')->nullable();
            $table->longText('invoiceUrl')->nullable();
            $table->longText('subscription')->nullable();
            $table->double('price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ngstripe_subscription_invoices');
    }
};
