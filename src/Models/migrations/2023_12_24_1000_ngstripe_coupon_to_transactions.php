<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('ngstripe_transactions', function (Blueprint $table) {
            $table->string('coupon')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('pdf_tracker')->nullable();
        });

        Schema::table('ngstripe_subscription_invoices', function (Blueprint $table) {
            $table->string('invoice_number')->nullable();
            $table->string('pdf_tracker')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('ngstripe_transactions', function(Blueprint $table){
            $table->dropColumn('coupon');
            $table->dropColumn('invoice_number');
            $table->dropColumn('pdf_tracker');
        });

        Schema::table('ngstripe_subscription_invoices', function(Blueprint $table){
            $table->dropColumn('invoice_number');
            $table->dropColumn('pdf_tracker');
        });
    }
};
