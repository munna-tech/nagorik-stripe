<?php

namespace Nagorik\Stripe\Models;

use Illuminate\Database\Eloquent\Model;

class NSUserActivity extends Model
{
    protected $table = 'ngstripe_user_activities';
}
