<?php

namespace Nagorik\Stripe\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use Nagorik\Stripe\Services\NgTransactionService;
use Nagorik\Stripe\Services\NgStripePaymentService;
use Nagorik\Stripe\Services\NgStripePaymentWebhook;

class NgStripeController extends Controller
{

    protected $ngStripePayment;
    protected $ngStripeWebhook;
    protected $ngTransactionService;

    public function __construct()
    {
        $this->ngStripePayment = new NgStripePaymentService();
        $this->ngStripeWebhook = new NgStripePaymentWebhook();
        $this->ngTransactionService = new NgTransactionService();
    }

    public function createPayment()
    {
        return view("ngstripe::console.panel");
    }

    public function paymentStatus($type, Request $get) {
        if ($type == 'success') {
            Session::put('payment_type', 'success');
            $txn = $this->ngStripePayment->processPaidPayment($get);
            $view = "ngstripe::payment.success";
            $params = "txn=".$txn->txn_id."&status=paid";

            if ($txn->status == 2) {
                Session::put('payment_type', 'error');
                $view = "ngstripe::payment.failed";
                $params = "txn=".$txn->txn_id."&status=failed";
            }
        }
        else {
            Session::put('payment_type', 'error');
            $txn = $this->ngStripePayment->processFailedPayment($get);
            $view = "ngstripe::payment.failed";
            $params = "txn=".$txn->txn_id."&status=failed";

            if ($txn->status == 1) {
                Session::put('payment_type', 'success');
                $view = "ngstripe::payment.success";
                $params = "txn=".$txn->txn_id."&status=paid";
            }
        }
        $redirect_url = config("ngstripe.payment.redirect_url");

        if ($redirect_url) {
            return redirect($redirect_url."?".$params);
        }
        $backUrl = config("ngstripe.payment.go_back_url") ?? "/";

        if ($txn == null)
        {
            return redirect($backUrl);
        }

        return view($view, ['txn' => $txn, "backUrl" => $backUrl]);
    }

    public function downloadInvoice($txnId) {
        $txn = $this->ngTransactionService->getByTxn($txnId);
        if ($txn == null || ($txn && $txn->pdf_tracker == null)) {
            return redirect("/?status=Transaction or invoice not found");
        }
        // $pdfText = str_replace("?s=ap", "", $txn->pdf_tracker);
        $pdfText = $txn->pdf_tracker;

        $invoiceUrl = "https://pay.stripe.com/invoice/".$pdfText;

        // dd($invoiceUrl);
        return redirect($invoiceUrl);

        // dd($invoiceUrl);

        // file name
        $filename = $txn->txn_id.".pdf";

        // dd($filename);

        // Download the PDF content
        $response = Http::get($invoiceUrl);

        // Check if the request was successful
        if ($response->successful()) {
            // Get the content of the PDF
            $pdfContent = $response->body();

            // Set the headers for the response
            $headers = [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename='.$filename,
            ];

            // Return the PDF as a response
            return response($pdfContent, 200, $headers);
        } else {
            // Handle the case where the request to the PDF URL was not successful
            return redirect("/?status=Failed to download the invoice");
        }
    }

    public function receieveWebhook(Request $request)
    {
        $eventName = $request['type'];
        logger("Receieved Webhook: ({$eventName}) ".json_encode($request->all()));

        try {
            $data = $request["data"];
            $eventName = $request['type'];
            $object = $data['object'];
            $id = $object['id'];

            if ($eventName == 'invoice.updated') {
                $this->ngStripeWebhook->invoiceUpdated($object, $id);
            }

            if ($eventName == 'invoice.sent') {
                $this->ngStripeWebhook->invoiceSent($object, $id);
            }

            if ($eventName == 'invoice.payment_failed') {
                $this->ngStripeWebhook->invoicePaymentFailed($object, $id);
            }

            if ($eventName == 'invoice.paid') {
                $this->ngStripeWebhook->invoicePaid($object, $id);
            }

            if ($eventName == 'invoice.payment_succeeded') {
                // logger("Received Webhook Package: ($eventName): ".json_encode($request->all()));
                $this->ngStripeWebhook->invoicePaymentSucceeded($object, $id);
            }

            if ($eventName == 'charge.succeeded') {
                $this->ngStripeWebhook->chargeSucceeded($object, $id);
            }

            if ($eventName == 'checkout.session.async_payment_succeeded') {
                $this->ngStripeWebhook->checkoutSessionAsyncPaymentSucceeded($object, $id);
            }

            if ($eventName == 'checkout.session.completed') {
                // logger("Received Webhook Package: ($eventName): ".json_encode($request->all()));
                $this->ngStripeWebhook->proccessWebhook($object, $id);
            }

            if ($eventName == 'customer.subscription.updated') {
                $this->ngStripeWebhook->customerSubscriptionUpdated($object, $id);
            }

            if ($eventName == 'customer.subscription.resumed') {
                $this->ngStripeWebhook->customerSubscriptionResumed($object, $id);
            }

            if ($eventName == 'subscription_schedule.created') {
                $this->ngStripeWebhook->subscriptionScheduleCreated($object, $id);
            }

            if ($eventName == 'subscription_schedule.updated') {
                $this->ngStripeWebhook->subscriptionScheduleUpdated($object, $id);
            }
        }
        catch (\Exception $e) {
            logger("Webhook stripe error: ".$e->getMessage());
        }
    }

    public function paymentJson() {
        $path = __DIR__."/../Json/invoice.json";
        $data = json_decode(file_get_contents($path), true);

        $object = $data["data"]["object"];
        $charge = $object["charge"];
        $subscription = $object["subscription"];
        $meta = $object["lines"]["data"][0]["plan"]["metadata"];
        $priceId = $object["lines"]["data"][0]["plan"]["id"];
        $amount = $object["lines"]["data"][0]["plan"]["amount"];
        $amount = (double) bcdiv($amount, 100, 2);
        $invoiceId = $object["id"];
        $hosted_invoice_url = $object["hosted_invoice_url"];
        $data = [
            "invoiceId" => $invoiceId,
            "price_id" => $priceId,
            "charge" => $charge,
            "amount" => $amount,
            "txn_id" => $meta["txn_id"],
            "app_prefix" => $meta["app_prefix"],
            "subscription" => $subscription,
            "invoiceUrl" => $hosted_invoice_url,
        ];
        return response()->json($data);
    }
}
