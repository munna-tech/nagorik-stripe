@extends('ngstripe::layouts.payment_status')

@section('title', 'Payment Success')

@section('content')
    <!-- component -->
    {{-- icon set --}}

    <div class="container">
        <div class="payment pay-success">
            <div class="text-center img-icon">
                <a href="{{ url('/') }}">
                    <img src="{{ asset(config('ngstripe.app_icon')) }}" class="h-28">
                </a>
            </div>
            <div class="box">
                <svg viewBox="0 0 24 24" class="text-green-600 w-16 h-16 mx-auto my-6">
                    <path fill="currentColor"
                        d="M12,0A12,12,0,1,0,24,12,12.014,12.014,0,0,0,12,0Zm6.927,8.2-6.845,9.289a1.011,1.011,0,0,1-1.43.188L5.764,13.769a1,1,0,1,1,1.25-1.562l4.076,3.261,6.227-8.451A1,1,0,1,1,18.927,8.2Z">
                    </path>
                </svg>
                <div class="text-center">
                    @if ($txn->is_subscription != true)
                    <h3 class="md:text-2xl text-base text-gray-900 font-semibold text-center">{{ config("ngstripe.message.payment.success.title") }}</h3>
                    <p class="text-gray-600 my-2">{{ config("ngstripe.message.payment.success.message") }}</p>
                    @else
                    <h3 class="md:text-2xl text-base text-gray-900 font-semibold text-center">{{ config("ngstripe.message.subscription.success.title") }}</h3>
                    <p class="text-gray-600 my-2">{{ config("ngstripe.message.subscription.success.message") }}</p>
                    @endif
                    <div class="payment-data">
                        <div class="pd-item">
                            <span>Payment Status</span>
                            <span class="color-green">Completed</span>
                        </div>
                        <div class="pd-item">
                            <span>Payment Type</span>
                            @if ($txn->is_subscription == true)
                                <span class="color-purple">
                                    Subscription
                                </span>
                            @else
                                <span>
                                    One Time
                                </span>
                            @endif
                        </div>
                        <div class="pd-item">
                            <span>Payment Time</span>
                            <span>{{ date('Y-m-d h:i a', strtotime($txn->created_at)) }}</span>
                        </div>
                        <div class="pd-item">
                            <span>Transaction ID</span>
                            <span>{{ $txn->txn_id }}</span>
                        </div>
                        <div class="pd-item">
                            <span>Invoice Number</span>
                            <span>{{ $txn->invoice_number }}</span>
                        </div>
                        <div class="pd-item">
                            <span>Coupon</span>
                            <span>{{ $txn->coupon ?? "N/A" }}</span>
                        </div>
                        <div class="pd-item">
                            <span>Email</span>
                            <span>{{ $txn->user->email }}</span>
                        </div>
                        <div class="pd-item">
                            <span>Amount</span>
                            <span>{{ $txn->getPrice() }} USD</span>
                        </div>
                        <div class="pd-item">
                            <span>Discount</span>
                            <span>{{ $txn->getDiscount() }} USD</span>
                        </div>
                        <div class="pd-item">
                            <span>Total</span>
                            <span>
                                <strong class="color-green">{{ $txn->getTotal() }} USD</strong>
                            </span>
                        </div>
                        @if ($txn->is_subscription == true)
                        <div class="pd-item">
                            <span>Invoice</span>
                            <span>
                                <strong class="color-green">
                                    <a href="{{ "https://pay.stripe.com/invoice/".$txn->pdf_tracker }}" target="_blank">
                                        Download
                                    </a>
                                </strong>
                            </span>
                        </div>
                        @endif

                    </div>
                    <div class="py-10 action-btn text-center">
                        <a href="{{ $backUrl }}" class="px-12 py-3">
                            GO BACK
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
