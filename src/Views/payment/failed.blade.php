@extends('ngstripe::layouts.payment_status')

@section('title', 'Payment Failed')

@section("bodyclass", "error")

@section('content')
    <!-- component -->
    {{-- icon set --}}

    <div class="container">
        <div class="payment pay-failed">
            <div class="text-center img-icon">
                <a href="{{ url('/') }}">
                    <img src="{{ asset(config('ngstripe.app_icon')) }}" class="h-28">
                </a>
            </div>
            <div class="box">
                <svg viewBox="0 0 24 24" class="text-green-600 w-16 h-16 mx-auto my-6">
                    <path fill="currentColor"
                        d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z">
                    </path>
                </svg>

                <div class="text-center">
                    @if ($txn->is_subscription != true)
                    <h3 class="md:text-2xl text-base text-gray-900 font-semibold text-center">{{ config("ngstripe.message.payment.failed.title") }}</h3>
                    <p class="text-gray-600 my-2">{{ config("ngstripe.message.payment.failed.message") }}</p>
                    @else
                    <h3 class="md:text-2xl text-base text-gray-900 font-semibold text-center">{{ config("ngstripe.message.subscription.failed.title") }}</h3>
                    <p class="text-gray-600 my-2">{{ config("ngstripe.message.subscription.failed.message") }}</p>
                    @endif
                    <div class="payment-data">
                        <div class="pd-item">
                            <span>Payment Status</span>
                            <span class="color-red">Failed</span>
                        </div>
                        <div class="pd-item">
                            <span>Payment Type</span>
                            @if ($txn->is_subscription == true)
                                <span class="color-purple">
                                    Subscription
                                </span>
                            @else
                                <span>
                                    One Time
                                </span>
                            @endif
                        </div>
                        <div class="pd-item">
                            <span>Payment Time</span>
                            <span>{{ date('Y-m-d h:i a', strtotime($txn->created_at)) }}</span>
                        </div>
                        <div class="pd-item">
                            <span>Transaction ID</span>
                            <span>{{ $txn->txn_id }}</span>
                        </div>
                        <div class="pd-item">
                            <span>Email</span>
                            <span>{{ $txn->user->email }}</span>
                        </div>
                        <div class="pd-item">
                            <span>Amount</span>
                            <span>{{ $txn->getPrice() }} USD</span>
                        </div>
                        <div class="pd-item">
                            <span>Discount</span>
                            <span>{{ $txn->getDiscount() }} USD</span>
                        </div>
                        <div class="pd-item">
                            <span>Total</span>
                            <span>
                                <strong class="color-red">{{ $txn->getTotal() }} USD</strong>
                            </span>
                        </div>
                    </div>
                    <div class="py-10 action-btn text-center">
                        <a href="{{ $backUrl }}" class="px-12 py-3">
                            GO BACK
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
