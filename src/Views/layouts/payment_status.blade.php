<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title') | {{ config('app.name') }}</title>
    <link rel="icon" href="{{ config('ngstripe.fav_icon') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('ngstripe/css/ngstripe_style.css', config('ngstripe.https')) }}" type="text/css">
    {{-- <link rel="stylesheet" href="https://bfc2-103-184-24-191.ngrok-free.app/ngstripe/css/ngstripe_style.css" type="text/css"> --}}
    @yield('css')
</head>

<body class="bg-gray-200 {{ session()->get('payment_type') }}">
    @yield('content')

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://code.jquery.com/jquery-3.7.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" crossorigin="anonymous">
    </script>
    @yield('js')
</body>

</html>
