<?php

namespace Nagorik\Stripe\Services;

use Illuminate\Support\Facades\DB;
use Nagorik\Stripe\Services\NgStripeResponse;
use Nagorik\Stripe\Exception\NgStripeException;
use Nagorik\Stripe\Services\NgStripeHttpService;
use Nagorik\Stripe\Services\NgTransactionService;
use Nagorik\Stripe\Services\NgSubscriptionService;

class NgStripeService
{
    // protected $baseUrl;
    // protected $apiKey;
    protected $host;
    protected $txnService;
    protected $subscriptionService;
    protected $httpService;

    public function __construct()
    {
        $this->host = config("ngstripe.base_url");

        $this->httpService = new NgStripeHttpService();
        $this->txnService = new NgTransactionService();
        $this->subscriptionService = new NgSubscriptionService();
    }

    public function createPayment($data): NgStripeResponse
    {
        // check if product exists
        if (!isset($data['package_id'])) {
            return NgStripeResponse::make(false, 'Package id is required');
        }

        // check if product exists
        if (!isset($data['user_id'])) {
            return NgStripeResponse::make(false, 'User id is required');
        }

        // check if product exists
        if (!isset($data['user_email'])) {
            return NgStripeResponse::make(false, 'User email is required');
        }

        // check if currency exists
        $currency = "usd";
        if (isset($data['currency'])) {
            $currency = $data["currency"];
        }

        $packageId = $data['package_id'];
        $getPackage = $this->getPackage($packageId);

        // check if price exists
        if (isset($data['price'])) {
            $price = (float) $data['price'];
        } else {
            $price = (float) $getPackage->price;
        }

        if (isset($data['name'])) {
            $name = $data['name'];
        } else if (isset($getPackage->name)) {
            $name = $getPackage->name;
        } else {
            $name = "Buy Package";
        }

        $redirect = $data['redirect'] ?? true;
        $coupon = $data['coupon'] ?? null;
        $userId = $data['user_id'];
        $userEmail = $data['user_email'];
        $discountAmount = 0;
        $total = $price;
        $discountType = "percentage";

        if (isset($data['discount_type'])) {
            $discountType = $data['discount_type'];
        }

        if (isset($data['coupon'])) {
            $coupon = $data['coupon'];
        }

        // check if product exists
        if (isset($data['discount'])) {
            $discountAmount = $this->getDiscount($price, $data['discount'], $discountType);
            $total = (float) bcsub($price, $discountAmount, 2);
        }

        // unit price
        $total = (int) bcmul($total, 100, 2);

        // create txn
        $txnData = [
            'userId' => $userId,
            'packageId' => $packageId,
            'price' => $price,
            'total' => $total,
            'coupon' => $coupon,
            'discount' => $discountAmount,
            'prod_id' => null, //$productId,
            'price_id' => null, //$priceId,
            'is_subscription' => false
        ];

        // create txn by request
        $txn = $this->txnService->create($txnData);

        $data = [
            'currency' => $currency,
            'unit_amount' => $total,
            'product_data' => [
                'name' => $name,
                // 'images' => [
                //     'https://securesharkvpn.com/wp-content/uploads/2023/12/cropped-3.png',
                // ]
            ],
            "metadata" => [
                "txn_id" => $txn->txn_id,
                "app_prefix" => $this->txnService->getTxnPrefix()
            ]
            // 'recurring' => ['interval' => 'month'],
        ];


        $request = $this->httpService->post('/v1/prices', $data);

        // dd($request);

        if ($request['status'] == false) {
            return NgStripeResponse::make(false, 'Failed to create payment link', ["data" => $request['message']]);
        }

        // return NgStripeResponse::make(true, 'Payment link has been created successfully', ["data" => $request['data']]);

        $getPrice = $request['data'];
        $priceId = $getPrice['id'];
        $productId = $getPrice['product'];

        // update txn by request product and price
        $this->txnService->updateProductPrice($txn, $priceId, $productId);

        $createPaymentUrl = $this->createPaymentUrl($priceId, $packageId, $userId, $userEmail, $currency, $txn->txn_id);

        if ($createPaymentUrl['status'] == false) {
            // dd($createPaymentUrl);
            return NgStripeResponse::make(false, 'Failed to create payment link', ["data" => $createPaymentUrl['message']]);
        }

        // dd($createPaymentUrl);

        $data = $createPaymentUrl['data'];
        $payID = $data['id'];

        $this->txnService->updatePayment($txn, $payID);

        return NgStripeResponse::make(true, 'Payment link has been created successfully', ["data" => $data, 'redirect' => $redirect]);
    }

    public function userId($id)
    {
        // make random id
        $userId = rand(100000, 999999) . "-" . $id . "-" . rand(100000, 999999);
        return $userId;
    }

    public function createPrice($data)
    {
        $request = $this->httpService->post('/v1/prices', $data);

        if ($request == false) {
            return false;
        }

        return $request;
    }

    public function createPaymentUrl($priceId, $packageId, $userId, $userEmail, $currency, $txnId)
    {
        // /v1/payment_links
        $request = $this->httpService->post('/v1/checkout/sessions', [
            'success_url' => $this->host . '/ngstripe/payment/success?p_id=' . $priceId . "&package_id=" . $packageId . "&u_id=" . $userId . "&txn_id=" . $txnId,
            'cancel_url' => $this->host . '/ngstripe/payment/cancel?p_id=' . $priceId . "&package_id=" . $packageId . "&u_id=" . $userId . "&txn_id=" . $txnId,
            'payment_method_types' => ['card'],
            'currency' => $currency,
            'customer_email' => $userEmail,
            'line_items' => [
                [
                    'price' => $priceId,
                    'quantity' => 1,
                ],
            ],
            "metadata" => [
                "txn_id" => $txnId,
                "app_prefix" => $this->txnService->getTxnPrefix()
            ],
            'mode' => 'payment'
        ]);

        return $request;
    }

    public function getPayment($payId)
    {
        // get checkout session
        $request = $this->httpService->get('/v1/checkout/sessions/' . $payId);

        if ($request['status'] == false) {
            return NgStripeResponse::make(false, 'Failed to get payment data', ["data" => $request['message']]);
        }

        return NgStripeResponse::make(true, 'Payment data has been retrieved', ["data" => $request['data']]);
    }

    public function getDiscount($price, $discount, $discountType)
    {
        if ($discountType == "percentage") {
            $discountAmount = (float) bcdiv(bcmul($price, $discount, 2), 100, 2);
        } else {
            $discountAmount = (float) $discount;
        }

        return $discountAmount;
    }

    public function getPackage($id)
    {
        $table = config("ngstripe.package.table");
        $primaryId = config("ngstripe.package.primary_key");
        try {
            $package = DB::table($table)->where($primaryId, $id)->first();
            if ($package == null) {
                throw new NgStripeException("Package id invalid", "Package not found or package id is invalid", 400);
            }
            return $package;
        } catch (\Exception $e) {
            throw new NgStripeException("Package id ({$id}) was invalid", "Package not found or package id is invalid, check primary key in your table and set primary key into the config file", 400);
        }
    }

    public function createSubscription($data)
    {
        // check if product exists
        if (!isset($data['package_id'])) {
            return NgStripeResponse::make(false, 'Package id is required');
        }

        // check if product exists
        if (!isset($data['user_id'])) {
            return NgStripeResponse::make(false, 'User id is required');
        }

        // check if product exists
        if (!isset($data['user_email'])) {
            return NgStripeResponse::make(false, 'User email is required');
        }

        // check if product exists
        if (!isset($data['recurring'])) {
            return NgStripeResponse::make(false, 'Recurring type is required');
        }

        // check if product exists
        if (!isset($data['recurring_period'])) {
            return NgStripeResponse::make(false, 'Recurring period/interval is required');
        }

        // check if currency exists
        $currency = "usd";
        if (isset($data['currency'])) {
            $currency = $data["currency"];
        }

        $packageId = $data['package_id'];
        $getPackage = $this->getPackage($packageId);

        // check if price exists
        if (isset($data['price'])) {
            $price = (float) $data['price'];
        } else {
            $price = (float) $getPackage->price;
        }

        if (isset($data['name'])) {
            $name = $data['name'];
        } else if (isset($getPackage->name)) {
            $name = $getPackage->name;
        } else {
            $name = "Buy Package";
        }

        $redirect = $data['redirect'] ?? true;
        $coupon = $data['coupon'] ?? null;
        $userId = $data['user_id'];
        $userEmail = $data['user_email'];
        $recurring = $data['recurring'];
        $recurring_period = $data['recurring_period'];
        $discountAmount = 0;
        $total = $price;
        $discountType = "percentage";

        if (isset($data['discount_type'])) {
            $discountType = $data['discount_type'];
        }

        if (isset($data['coupon'])) {
            $coupon = $data['coupon'];
        }

        $recurringAllowed = ['day', 'week', 'month', 'year'];
        if (!in_array($recurring, $recurringAllowed)) {
            return NgStripeResponse::make(false, 'Recurring type is invalid');
        }

        if ($recurring_period < 1) {
            return NgStripeResponse::make(false, 'Recurring period/interval is invalid');
        }

        // check if product exists
        if (isset($data['discount'])) {
            $discountAmount = $this->getDiscount($price, $data['discount'], $discountType);
            $total = (float) bcsub($price, $discountAmount, 2);
        }

        // unit price
        $total = (int) bcmul($total, 100, 2);

        // create txn
        $txnData = [
            'userId' => $userId,
            'packageId' => $packageId,
            'price' => $price,
            'coupon' => $coupon,
            'total' => $total,
            'discount' => $discountAmount,
            'prod_id' => null, // $productId,
            'price_id' => null, // $priceId,
            'is_subscription' => true,
            'recurring' => [
                'interval' => $recurring,
                'interval_count' => $recurring_period
            ]
        ];
        $txn = $this->txnService->create($txnData);

        $data = [
            'currency' => $currency,
            'unit_amount' => $total,
            'product_data' => ['name' => $name],
            'recurring' => [
                'interval' => $recurring,
                'interval_count' => $recurring_period
            ],
            "metadata" => [
                "txn_id" => $txn->txn_id,
                "app_prefix" => $this->txnService->getTxnPrefix()
            ]
            // 'recurring' => ['interval' => 'month'],
        ];

        // dd($data);
        $request = $this->httpService->post('/v1/prices', $data);
        // dd($request);

        if ($request['status'] == false) {
            return NgStripeResponse::make(false, 'Failed to create subscription link', ["data" => $request['message']]);
        }

        // return NgStripeResponse::make(true, 'Payment link has been created successfully', ["data" => $request['data']]);

        $getPrice = $request['data'];
        $priceId = $getPrice['id'];
        $productId = $getPrice['product'];

        // update price informations
        $this->txnService->updateProductPrice($txn, $priceId, $productId);


        $createPaymentUrl = $this->createSubscriptionUrl($priceId, $packageId, $userId, $userEmail, $currency, $txn->txn_id);

        if ($createPaymentUrl['status'] == false) {
            // dd($createPaymentUrl);
            return NgStripeResponse::make(false, 'Failed to create subscription link', ["data" => $createPaymentUrl['message']]);
        }

        // dd($createPaymentUrl);

        $data = $createPaymentUrl['data'];
        $payID = $data['id'];

        // $this->txnService->updatePayment($txn->txn_id, $payID);
        $this->txnService->updatePayment($txn, $payID);

        return NgStripeResponse::make(true, 'Subscription link has been created successfully', ["data" => $data, 'redirect' => $redirect]);
    }

    public function createSubscriptionUrl($priceId, $packageId, $userId, $userEmail, $currency, $txnId)
    {
        // /v1/payment_links
        $request = $this->httpService->post('/v1/checkout/sessions', [
            'success_url' => $this->host . '/ngstripe/payment/success?p_id=' . $priceId . "&package_id=" . $packageId . "&u_id=" . $userId . "&txn_id=" . $txnId,
            'cancel_url' => $this->host . '/ngstripe/payment/cancel?p_id=' . $priceId . "&package_id=" . $packageId . "&u_id=" . $userId . "&txn_id=" . $txnId,
            'payment_method_types' => ['card'],
            'currency' => $currency,
            'customer_email' => $userEmail,
            'line_items' => [
                [
                    'price' => $priceId,
                    'quantity' => 1,
                ],
            ],
            "metadata" => [
                "txn_id" => $txnId,
                "app_prefix" => $this->txnService->getTxnPrefix()
            ],
            'mode' => 'subscription'
        ]);

        return $request;
    }

    public function cancelSubscription($subscriptionId) {
        $url = "/v1/subscriptions";
        $request = $this->httpService->delete($url."/".$subscriptionId);

        // logger("SUbscription delete");
        // logger($request);

        if ($request['status'] == false) {
            return false;
        }

        return true;
    }

    public function moveToCancelledSubscription($subscription) {
        $this->txnService->moveToCancelledSubscription($subscription);
    }
}
