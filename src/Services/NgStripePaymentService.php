<?php

namespace Nagorik\Stripe\Services;

use App\Models\User;
use Nagorik\Stripe\Services\NgStripeService;
use Nagorik\Stripe\Services\NgTransactionService;
use Nagorik\Stripe\Services\NgUserSubscriptionService;

class NgStripePaymentService
{

    protected $ngTransactionService;
    protected $ngStripeService;
    protected $ngUserSubscriptionService;

    public function __construct()
    {
        $this->ngTransactionService = new NgTransactionService();
        $this->ngStripeService = new NgStripeService();
        $this->ngUserSubscriptionService = new NgUserSubscriptionService();
    }

    public function processPaidPayment($get)
    {
        $p_id = $get->p_id;
        $package_id = $get->package_id;
        $u_id = $get->u_id;
        $txn_id = $get->txn_id;

        $txn = $this->ngTransactionService->getByTxn($txn_id);
        if ($txn) {
            if ($txn->status != 0) {
                // return ... because already paid or cancelled
                return $txn;
            }

            // check txn paid validity
            $payId = $txn->st_id;
            $txn->status = 1;
            $txn->is_expired = 0;
            $txn->updated_at = now();
            $txn->save();

            // update subscription to user subscription
            $this->ngUserSubscriptionService->makeSubscription($txn, $package_id, $u_id);

            // call an command to update user package
            $notifyData = [
                'status' => 'paid',
                'type' => 'payment',
                'user_id' => $u_id,
                'package_id' => $package_id,
                'txn_id' => $txn_id,
                'pay_id' => $payId
            ];

            $this->notifyData($notifyData);

            return $txn;
        }

        // txn not found
        logger("Txn not found: " . json_encode($get->all()));
        return null;
    }

    public function processFailedPayment($get)
    {
        $p_id = $get->p_id;
        $package_id = $get->package_id;
        $u_id = $get->u_id;
        $txn_id = $get->txn_id;

        $txn = $this->ngTransactionService->getByTxn($txn_id);
        if ($txn) {
            if ($txn->status != 0) {
                // return ... because already paid
                return $txn;
            }

            // check txn paid validity
            $payId = $txn->st_id;
            $txn->status = 2;
            $txn->updated_at = now();
            $txn->save();

            // transfer to failed txn table
            // $txn = $this->ngTransactionService->transferToFailed($txn);

            // call an command to update user package
            $notifyData = [
                'status' => 'failed',
                'type' => 'payment',
                'user_id' => $u_id,
                'package_id' => $package_id,
                'txn_id' => $txn_id,
                'pay_id' => $payId
            ];

            $this->notifyData($notifyData);
            return $txn;
        }

        // txn not found
        logger("Txn not found: " . json_encode($get->all()));
        return null;
    }

    public function notifyData($notifyData)
    {
        $notifyClass = config('ngstripe.notify.class');
        $notifyMethod = config('ngstripe.notify.method');

        // if class exists check
        if (!class_exists($notifyClass)) {
            return;
        }

        // method is exists or not
        if (!method_exists($notifyClass, $notifyMethod)) {
            return;
        }

        $ncInstance = new $notifyClass();
        $ncInstance->$notifyMethod($notifyData);
    }
}
