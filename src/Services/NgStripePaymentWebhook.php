<?php
namespace Nagorik\Stripe\Services;

use Carbon\Carbon;
use App\Models\User;
use Nagorik\Stripe\Services\NgTransactionService;
use Nagorik\Stripe\Services\NgUserSubscriptionService;

class NgStripePaymentWebhook {

    protected $ngTransactionService;
    protected $ngUserSubscriptionService;

    public function __construct()
    {
        $this->ngTransactionService = new NgTransactionService();
        $this->ngUserSubscriptionService = new NgUserSubscriptionService();
    }

    public function proccessWebhook($object, $id) {
        $email = $object["customer_email"];
        $invoice = $object["invoice"];
        $payment_status = $object["payment_status"];
        $amount = $object["amount_total"] / 100;

        $txn = $this->ngTransactionService->getByPayId($id);
        if ($txn) {
            $txn->data = json_encode($object);
            $txn->save();

            $txnType = $txn->is_subscription ? "subscription" : "payment";
            if ($txnType == "payment") {
                // no need to update payment
                return;
            }

            // update subscription
            $user = User::find($txn->user_id);
            if ($user) {
                $this->ngUserSubscriptionService->updateSubscription($txn, $user);
            }
        }
    }

    public function invoiceUpdated($object, $id)
    {

    }

    public function invoiceSent($object, $id)
    {

    }

    public function invoicePaymentFailed($object, $id)
    {

    }

    public function invoicePaid($object, $id)
    {

    }

    public function invoicePaymentSucceeded($object, $id)
    {
        // logger($object);
        // $object = $object["data"]["object"];
        $charge = $object["charge"];
        $invoiceNumber = $object["number"];
        $invoicePdf = $object["invoice_pdf"];
        $subscription = $object["subscription"];
        $meta = $object["lines"]["data"][0]["plan"]["metadata"];
        $priceId = $object["lines"]["data"][0]["plan"]["id"];
        $amount = $object["lines"]["data"][0]["plan"]["amount"];
        $amount = (double) bcdiv($amount, 100, 2);
        $invoiceId = $object["id"];
        $hosted_invoice_url = $object["hosted_invoice_url"];

        $getAppPrefix = config("ngstripe.payment.txn_prefix");
        if ($getAppPrefix != $meta["app_prefix"]) {
            // get app prefix ...
            return;
        }

        // logger("Got invoiceNumber: ".$invoiceNumber);

        $data = [
            "invoiceId" => $invoiceId,
            "invoicePdf" => $invoicePdf,
            "invoiceNumber" => $invoiceNumber,
            "price_id" => $priceId,
            "charge" => $charge,
            "amount" => $amount,
            "txn_id" => $meta["txn_id"],
            "app_prefix" => $meta["app_prefix"],
            "subscription" => $subscription,
            "invoiceUrl" => $hosted_invoice_url,
        ];

        $txn = $this->ngTransactionService->getByTxn($meta["txn_id"]);

        if (!$txn) {
            return;
        }

        $pdf_tracker = $this->pdfTraceNumber($invoicePdf);

        // update the transactions
        $txn->is_expired = 0;
        $txn->invoice_number = $invoiceNumber ?? null;
        $txn->pdf_tracker = $pdf_tracker ?? null;
        $txn->updated_at = Carbon::now();
        $txn->save();

        // create transaction service
        $createInvoice = $this->ngTransactionService->createInvoice($txn, $data);

        // update user subscription
        if ($createInvoice != null)
        {
            $this->ngUserSubscriptionService->updateSubscription($txn, $createInvoice);
            // sent notification
            $this->sendWebhookNotify($data, $txn->user_id, $txn->package_id);
        }
    }

    public function pdfTraceNumber($pdfUrl) {
        // slice the pdf url and get the last part
        $data = str_replace("https://pay.stripe.com/invoice/", "", $pdfUrl);
        return $data;
    }

    public function chargeSucceeded($object, $id)
    {

    }

    public function checkoutSessionAsyncPaymentSucceeded($object, $id)
    {

    }

    public function customerSubscriptionUpdated($object, $id)
    {
        logger("Subscription Updated: ".json_encode($object));
    }

    public function customerSubscriptionResumed($object, $id)
    {

    }

    public function subscriptionScheduleCreated($object, $id)
    {

    }

    public function subscriptionScheduleUpdated($object, $id)
    {

    }

    public function sendWebhookNotify($data, $userId, $packageId) {
        // call an command to update user package
        $notifyClass = config('ngstripe.webhook.class');
        $notifyMethod = config('ngstripe.webhook.method');

        // check class exists or not
        if (!class_exists($notifyClass)) {
            return;
        }

        // if method not exists then return
        if (!method_exists($notifyClass, $notifyMethod)) {
            return;
        }

        $ncInstance = new $notifyClass();
        $ncInstance->$notifyMethod($data);
    }
}
