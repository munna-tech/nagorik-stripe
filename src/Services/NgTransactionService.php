<?php

namespace Nagorik\Stripe\Services;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Nagorik\Stripe\Models\NSTransaction;
use Nagorik\Stripe\Models\NSFailedTransaction;
use Nagorik\Stripe\Models\NSSubscriptionInvoice;
use Nagorik\Stripe\Models\NSUserActivity;
use Nagorik\Stripe\Models\NSUserCancelledSubscription;

class NgTransactionService
{

    public function __construct()
    {
        //
    }

    public function create($data)
    {
        $txnPrefix = $this->getTxnPrefix();

        $txn_id = $txnPrefix."-".Str::random(10);

        $txn = new NSTransaction();
        $txn->txn_id = $txn_id;
        $txn->user_id = $data['userId'];
        $txn->package_id = $data['packageId'];
        $txn->price = $data['price'];
        $txn->total = $data['total'];
        $txn->coupon = $data['coupon'];
        $txn->discount = $data['discount'];
        $txn->prod_id = null; //$data['prod_id'];
        $txn->price_id = null; //$data['price_id'];
        $txn->st_id = null;
        $txn->is_subscription = $data['is_subscription'] ?? false;
        if ($data['is_subscription'] == true) {
            $txn->recurring = json_encode($data['recurring']);
        }
        $txn->save();

        return $txn;
    }

    public function getTxnPrefix() {
        $prefix = config("ngstripe.payment.txn_prefix");
        if (!$prefix)
        {
            $appName = config("app.name");
            if ($appName) {
                $prefix = Str::slug($appName);
            }
            else
            {
                $prefix = "ns_txn";
            }
        }

        $prefix = str_replace("-", "_", $prefix);
        $prefix = str_replace("?", "_", $prefix);
        $prefix = str_replace("&", "_", $prefix);
        $prefix = str_replace("#", "_", $prefix);
        $prefix = str_replace("/", "_", $prefix);
        $prefix = strtolower($prefix);
        return $prefix;
    }

    public function isMyTxn($txn) {
        $split = explode("-", $txn);
        $pre = $split[0];
        $getPrefix = $this->getTxnPrefix();

        if ($pre == $getPrefix) {
            return true;
        }
        return false;
    }

    public function updatePayment($txn, $st_id) {
        // $txn = NSTransaction::where('txn_id', $txnId)->first();
        $txn->st_id = $st_id;
        $txn->save();
        return $txn;
    }

    public function updateProductPrice($txn, $priceId, $productId) {
        // $txn = NSTransaction::where('txn_id', $txnId)->first();
        $txn->prod_id = $priceId;
        $txn->price_id = $productId;
        $txn->save();

        return $txn;
    }

    public function getByTxn($txnId) {
        $txn = NSTransaction::where('txn_id', $txnId)->first();
        return $txn;
    }

    public function getByPayId($txnId) {
        $txn = NSTransaction::where('st_id', $txnId)->first();
        return $txn;
    }

    public function transferToFailed($txn)
    {
        // copy $txn all data to failed txn
        $failedTxn = new NSFailedTransaction();
        foreach ($txn->toArray() as $key => $value) {
            $failedTxn->$key = $value;
        }
        $failedTxn->save();

        $txn->delete();
        return $failedTxn;
    }

    public function createInvoice($txn, $data) {
        // $data = [
        //     "invoiceId" => $invoiceId,
        //     "price_id" => $priceId,
        //     "charge" => $charge,
        //     "amount" => $amount,
        //     "txn_id" => $meta["txn_id"],
        //     "app_prefix" => $meta["app_prefix"],
        //     "subscription" => $subscription,
        //     "invoiceUrl" => $hosted_invoice_url,
        // ];

        $checkExists = NSSubscriptionInvoice::where('user_id', $txn->user_id)->where('st_id', $txn->st_id)->where('invoice_id', $data['invoiceId'])->where('price_id', $data['price_id'])->first();
        if ($checkExists) {
            // its already exists
            return;
        }

        // create invoice
        $invoice = new NSSubscriptionInvoice();
        $invoice->user_id = $txn->user_id;
        $invoice->package_id = $txn->package_id;
        $invoice->st_id = $txn->st_id;
        $invoice->invoice_id = $data['invoiceId'];
        $invoice->invoice_number = $data['invoiceNumber'];
        $invoice->price_id = $data['price_id'];
        $invoice->price = $data['amount'];
        $invoice->invoiceUrl = $data['invoiceUrl'];
        $invoice->subscription = $data['subscription'];
        $invoice->created_at = now()->toDateTimeString();
        $invoice->save();

        // update the subscription id
        $txn->subscription_id = $data['subscription'];
        $txn->updated_at = now()->toDateTimeString();
        $txn->save();

        return $invoice;
    }

    public function moveToCancelledSubscription($old) {
        $subscription = new NSUserCancelledSubscription();
        $subscription->user_id = $old->user_id;
        $subscription->package_id = $old->package_id;
        $subscription->created_at = $old->created_at;
        $subscription->start_at = $old->start_at;
        $subscription->ends_at = $old->ends_at;
        $subscription->cancelled_at = Carbon::now();
        $subscription->is_active = $old->is_active;
        $subscription->subscription = $old->subscription;
        $subscription->status = $old->status;
        $subscription->save();
    }

    public function addActivity($userId, $title, $message) {
        $activity = new NSUserActivity();
        $activity->user_id = $userId;
        $activity->title = $title;
        $activity->message = $message;
        $activity->status = 1;
        $activity->created_at = Carbon::now();
        $activity->save();
    }
}
