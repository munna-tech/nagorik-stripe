<?php

namespace Nagorik\Stripe\Services;

use Carbon\Carbon;
use App\Models\User;
use Nagorik\Stripe\Models\NSTransaction;
use Nagorik\Stripe\Services\NgStripeService;
use Nagorik\Stripe\Models\NSUserSubscription;
use Nagorik\Stripe\Models\NSSubscriptionInvoice;

class NgUserSubscriptionService
{
    public function updateSubscription($txn, $invoice)
    {
        $userId = $txn->user_id;
        $subscription = NSUserSubscription::where("user_id", $userId)->where("package_id", $txn->package_id)->where("subscription", $txn->subscription_id)->first();

        if ($subscription) {
            $this->updateSubscriptionItem($subscription, $txn);
        } else {
            // check if subscription id is empty
            $subscription = NSUserSubscription::where("user_id", $userId)->where("package_id", $txn->package_id)->first();
            if ($subscription->subscription == null || $subscription->subscription == '') {
                $id = $txn->subscription_id;
                $this->updateSubscriptionId($subscription, $txn, $id);
            }
            else {
                $this->createSubscriptionItem($txn, $userId, $txn->package_id);
            }
        }
    }

    public function makeSubscription($txn, $package_id, $u_id)
    {

        if ($txn->is_subscription == 0) {
            // its only for subscription
            return;
        }

        $userId = $u_id;
        $getUId = $txn->user_id;

        if ($userId != $getUId) {
            // on webhook user id not matched
            logger("On webhook user not matched: txn_id=" . $txn->txn_id);
            return;
        }

        $user = User::find($u_id);
        if (!$user) {
            logger("On webhook user not found: txn_id=" . $txn->txn_id);
            return;
        }

        $subscription = NSUserSubscription::where("user_id", $user->id)->where("package_id", $package_id)->first();

        if ($subscription) {
            $this->updateSubscriptionItem($subscription, $txn);
        } else {
            $this->createSubscriptionItem($txn, $user->id, $package_id);
        }
    }

    public function updateSubscriptionItem($subscription, $txn)
    {
        $subscription->is_active = 1;
        $subscription->status = 1;
        // $subscription->is_expired = 0;
        $subscription->start_at = Carbon::now()->toDateTimeString();
        $subscription->ends_at = $this->endDate($txn);
        $subscription->save();
    }

    public function createSubscriptionItem($txn, $userId, $package_id)
    {
        $subscription = new NSUserSubscription();
        $subscription->user_id = $userId;
        $subscription->package_id = $package_id;
        $subscription->created_at = now()->toDateTimeString();
        $subscription->start_at = now()->toDateTimeString();
        $subscription->ends_at = $this->endDate($txn);
        $subscription->is_active = 1;
        $subscription->subscription = $txn->subscription_id;
        $subscription->status = 1;
        $subscription->save();
    }

    public function updateSubscriptionId($subscription, $txn, $id) {
        $subscription->is_active = 1;
        $subscription->status = 1;
        // $subscription->is_expired = 0;
        $subscription->start_at = Carbon::now()->toDateTimeString();
        $subscription->ends_at = $this->endDate($txn);
        $subscription->subscription = $id;
        $subscription->save();
    }

    public function endDate($txn)
    {
        if ($txn->is_subscription == 1) {
            $recurring = json_decode($txn->recurring, true);
            // dd([$recurring, gettype($recurring), $txn->recurring]);
            $interval = $recurring["interval"];
            $interval_count = $recurring["interval_count"];

            $now = null;

            if ($interval == "day") {
                $now = Carbon::now()->addDays($interval_count);
            } else if ($interval == "month") {
                $now = Carbon::now()->addMonths($interval_count);
            } else if ($interval == "year") {
                $now = Carbon::now()->addYears($interval_count);
            }

            if ($now) {
                return $now->toDateTimeString();
            }
        }

        return null;
    }

    public function getSubscription($userId)
    {
        return NSUserSubscription::where("user_id", $userId)->orderBy('id', 'desc')->select("user_id", "package_id", "is_active", "start_at", "ends_at", "status")->get();
    }

    public function userPackageSubscription($userId, $packId)
    {
        return NSUserSubscription::where("user_id", $userId)->where("package_id", $packId)->orderBy('id', 'desc')->select("user_id", "package_id", "is_active", "start_at", "ends_at", "status")->first();
    }

    public function isSubscriber($userId, $packageId = null)
    {
        $find = false;

        if ($packageId != null) {
            $getSubscription = NSUserSubscription::where("user_id", $userId)->where("package_id", $packageId)->where('is_active', true)->first();
            if ($getSubscription) {
                $find = true;
            }
        } else {
            $getSubscription = NSUserSubscription::where("user_id", $userId)->where('is_active', true)->get();
            if ($getSubscription->count() > 0) {
                $find = true;
            }
        }

        return $find;
    }

    public function userTransactions($userId, $packageId = null)
    {
        if ($packageId != null) {
            $txn = NSTransaction::where("user_id", $userId)->where("package_id", $packageId)->orderBy('id', 'desc')->get();
            return $txn;
        } else {
            $txn = NSTransaction::where("user_id", $userId)->orderBy('id', 'desc')->get();
            return $txn;
        }
    }

    public function userInvoices($userId, $packageId = null)
    {
        if ($packageId != null) {
            $txn = NSSubscriptionInvoice::where("user_id", $userId)->where("package_id", $packageId)->orderBy('id', 'desc')->get();
            return $txn;
        } else {
            $txn = NSSubscriptionInvoice::where("user_id", $userId)->orderBy('id', 'desc')->get();
            return $txn;
        }
    }


    public function userPaymentTransactions($userId, $packageId = null)
    {
        if ($packageId != null) {
            $txn = NSTransaction::where("user_id", $userId)->where('is_subscription', 0)->where("package_id", $packageId)->orderBy('id', 'desc')->get();
            return $txn;
        } else {
            $txn = NSTransaction::where("user_id", $userId)->where('is_subscription', 0)->orderBy('id', 'desc')->get();
            return $txn;
        }
    }

    public function userSubscriptionTransactions($userId, $packageId = null)
    {
        if ($packageId != null) {
            $txn = NSTransaction::where("user_id", $userId)->where('is_subscription', 1)->where("package_id", $packageId)->orderBy('id', 'desc')->get();
            return $txn;
        } else {
            $txn = NSTransaction::where("user_id", $userId)->where('is_subscription', 1)->orderBy('id', 'desc')->get();
            return $txn;
        }
    }

    public function cancelAllSubscription($userId)
    {
        $subscriptionList = NSUserSubscription::where("user_id", $userId)->get();
        $stripe = new NgStripeService();

        $total = 0;
        $totalFailed = 0;

        foreach ($subscriptionList as $list)
        {
            $subscription = $list->subscription;
            $cancelStatus = $stripe->cancelSubscription($subscription);

            if ($cancelStatus == true) {
                $stripe->moveToCancelledSubscription($list);
                $list->delete();
                $total++;
            }
            else
            {
                $totalFailed++;
            }
        }

        return [
            "total_subscription" => $subscriptionList->count(),
            "total_cancelled" => $total,
            "total_cancelled_failed" => $totalFailed,
        ];
    }

    public function cancelPackageSubscription($userId, $packageId)
    {
        $subscriptionList = NSUserSubscription::where("user_id", $userId)->where("package_id", $packageId)->get();
        $stripe = new NgStripeService();

        $total = 0;
        $totalFailed = 0;

        foreach ($subscriptionList as $list)
        {
            $subscription = $list->subscription;
            $cancelStatus = $stripe->cancelSubscription($subscription);

            if ($cancelStatus == true) {
                $stripe->moveToCancelledSubscription($list);
                $list->delete();
                $total++;
            }
            else
            {
                $totalFailed++;
            }
        }

        return [
            "total_subscription" => $subscriptionList->count(),
            "total_cancelled" => $total,
            "total_cancelled_failed" => $totalFailed,
        ];
    }

    public function cancelSubscription($userId, $subscription)
    {
        $subscriptionList = NSUserSubscription::where("user_id", $userId)->where("subscription", $subscription)->first();
        $stripe = new NgStripeService();

        if ($subscriptionList) {
            $cancelStatus = $stripe->cancelSubscription($subscriptionList->subscription);
            if ($cancelStatus == true) {
                $stripe->moveToCancelledSubscription($subscriptionList);
                $subscriptionList->delete();
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function getTxn($txn_id)
    {
        return NSTransaction::where("txn_id", $txn_id)->first();
    }

    public function getSubscriptionDetails($subscription_id)
    {
        return NSUserSubscription::where("subscription", $subscription_id)->first();
    }

    public function getInvoice($invoice_id)
    {
        return NSSubscriptionInvoice::where("invoice_id", $invoice_id)->first();
    }

    public function getTxnByCoupon($coupon, $userId = null)
    {
        if ($coupon != null) {
            if ($userId) {
                $txn = NSTransaction::where("user_id", $userId)->where("coupon", $coupon)->orderBy('id', 'desc')->get();
            }
            else {
                $txn = NSTransaction::where("coupon", $coupon)->orderBy('id', 'desc')->get();
            }
            return $txn;
            // $txn = NSTransaction::where("user_id", $userId)->where('is_subscription', 0)->where("package_id", $packageId)->orderBy('id', 'desc')->get();
        } else {
            return null;
        }
    }

    public function downloadInvoice($txnId)
    {
        $txn = $this->getTxn($txnId);

        if ($txn == null || ($txn && $txn->pdf_tracker == null)) {
            return null;
        }
        // $pdfText = str_replace("?s=ap", "", $txn->pdf_tracker);
        $pdfText = $txn->pdf_tracker;

        $invoiceUrl = "https://pay.stripe.com/invoice/".$pdfText;
        return $invoiceUrl;
    }

    public function txnLists($status = null, $paginate = true, $perPage = 10)
    {

        if ($status) {
            if ($paginate) {
                $txn = NSTransaction::where("status", $status)->orderBy('id', 'desc')->paginate($perPage);
            }
            else {
                $txn = NSTransaction::where("status", $status)->orderBy('id', 'desc')->get();
            }
        }
        else {
            if ($paginate) {
                $txn = NSTransaction::orderBy('id', 'desc')->paginate($perPage);
            }
            else {
                $txn = NSTransaction::orderBy('id', 'desc')->get();
            }
        }

        return $txn;
    }
}
