<?php

namespace Nagorik\Stripe\Services;

use stdClass;

class NgStripeResponse
{
    public static $status;
    public static $message;
    public static $data;

    public function __construct()
    {
    }

    public static function make($status, $message = 'Success', $data = [])
    {
        self::$status = $status;
        self::$message = $message;
        self::$data = $data;

        return new self();
    }

    public function return()
    {
        return response()->json([
            'status' => self::$status,
            'message' => self::$message,
            'data' => self::$data
        ]);
    }

    public function data()
    {
        $dataObject = new stdClass;
        $dataObject->status = self::$status;
        $dataObject->message = self::$message;
        if (isset(self::$data["data"])) {
            $stdata = json_decode(json_encode(self::$data["data"]));
            $dataObject->data = $stdata;
        }
        return $dataObject;
    }
}
