<?php

namespace Nagorik\Stripe\Services;

use Nagorik\Stripe\Services\NgStripeHttpService;

class NgStripeWebhookService
{
    protected $httpService, $host;
    protected $webhookApi = "/ngstripe/webhook";

    public function __construct()
    {
        $this->httpService = new NgStripeHttpService();
        $this->host = config("ngstripe.base_url");
    }

    public function createWebhooks() {
        $data = [
            'enabled_events' => [
                'invoice.created',
                'invoice.updated',
                'invoice.sent',
                'invoice.payment_failed',
                'invoice.paid',
                'invoice.upcoming',
                'invoice.payment_succeeded',
                'charge.succeeded',
                'checkout.session.async_payment_succeeded',
                'checkout.session.completed',
                'customer.subscription.updated',
                'customer.subscription.resumed',
                'checkout.session.async_payment_succeeded',
                'subscription_schedule.created',
                'subscription_schedule.updated'
            ],
            'url' => $this->host.$this->webhookApi,
        ];
        $createWebhook = $this->httpService->post("/v1/webhook_endpoints", $data);
        if (!$createWebhook['status']) {
            return null;
        }
        return $createWebhook['data'];
    }

    public function getTheListOfWebhooks() {
        $webhooks = $this->httpService->get("/v1/webhook_endpoints?limit=100");
        if (!$webhooks['status']) {
            return null;
        }
        return $webhooks['data'];
    }

    public function checkWebHookExists() {
        $lists = $this->getTheListOfWebhooks();

        if ($lists == null) {
            return null;
        }

        $webhook = collect($lists['data'])->where('url', $this->host.$this->webhookApi)->first();
        if ($webhook) {
            return $webhook;
        }
        return null;
    }

    public function removeWebHook() {
        $lists = $this->getTheListOfWebhooks();

        if ($lists == null) {
            return false;
        }

        $webhook = collect($lists['data'])->where('url', $this->host.$this->webhookApi)->first();
        if ($webhook) {
            $id = $webhook["id"];
            $webhookRemove = $this->httpService->delete("/v1/webhook_endpoints/".$id);
            if (!$webhookRemove['status']) {
                return false;
            }
            return true;
        }
        else
        {
            $id = "we_1OIb0tLRN9lOZ6qktlsDKAdK";
            $webhookRemove = $this->httpService->delete("/v1/webhook_endpoints/".$id);
            if (!$webhookRemove['status']) {
                return false;
            }
            return true;
        }
        return false;
    }
}
