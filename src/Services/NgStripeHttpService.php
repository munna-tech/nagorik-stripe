<?php

namespace Nagorik\Stripe\Services;

use Illuminate\Support\Facades\Http;

class NgStripeHttpService
{
    protected $baseUrl;
    protected $apiKey;

    public function __construct()
    {
        $this->baseUrl = "https://api.stripe.com";
        $this->apiKey = config("ngstripe.payment.api_key");
    }

    public function post($api, $data = [])
    {
        $apiUrl = $this->baseUrl . $api;
        $http = Http::withToken($this->apiKey)->asForm()->post($apiUrl, $data);

        // if req failed return here
        if ($http->failed()) {
            return ['status' => false, 'message' => $http->json()['error']['message']];
        }

        // return response
        return ['status' => true, 'message' => 'Request has been successful', 'data' => $http->json()];
    }

    public function get($api)
    {
        $apiUrl = $this->baseUrl . $api;
        $http = Http::withToken($this->apiKey)->asForm()->get($apiUrl);

        // if req failed return here
        if ($http->failed()) {
            return ['status' => false, 'message' => $http->json()['error']['message']];
        }

        // return response
        return ['status' => true, 'message' => 'Request has been successful', 'data' => $http->json()];
    }

    public function delete($api)
    {
        $apiUrl = $this->baseUrl . $api;
        $http = Http::withToken($this->apiKey)->asForm()->delete($apiUrl);

        // if req failed return here
        if ($http->failed()) {
            return ['status' => false, 'message' => $http->json()['error']['message']];
        }

        // return response
        return ['status' => true, 'message' => 'Request has been successful', 'data' => $http->json()];
    }
}
