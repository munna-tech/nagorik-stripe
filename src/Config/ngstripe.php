<?php

return [
    /**
    * Set pacakge base url
    * Example: https://example.com
    * Your hosted url where you want to use this package
    */
    "base_url" => env("BASE_URL", "https://4125-103-203-93-212.ngrok-free.app"),

    /**
    * Set app icon
    * Example: /ngstripe/icons/favicon.png
    * Favicon path
    */
    "app_icon" => "/ngstripe/icons/favicon.png",
    "fav_icon" => "/ngstripe/icons/favicon.png",

    /**
    * Assets/resource path will use https if enable
    * Redirection will use https if enable
    */
    "https" => true,

    /**
    * Set payment information
    * api_key: Stripe api key
    * redirect_url: After payment success where to redirect default is null
    * go_back_url: After payment where to redirect by click on back button on payment success/failed page. default is /
    * txn_prefix: Its very important. If you have multiple project on same stripe account then you must set this. Its should be unique for each project.
    * subscription_holding_days: After subscription expired, how many days you want to hold the subscription. default is 1 day
    */

    "payment" => [
        "api_key" => "sk_test_51MEW0yLRN9lOZ6qkcEBkuprYMsIcT6QpYCdzStochtKN1KXlIX7WOgyaUWHLUHOutUSXWARpGLZl69Es71BMOYuW00YyjddSYY",
        "redirect_url" => null,
        "go_back_url" => "/",
        "txn_prefix" => "ns_vpn",
        "subscription_holding_days" => 1
    ],

    /**
    * Notificaiton settings
    * class: Class name with namespace
    * method: Method name
    * This method will call after payment success/failed
    */
    "notify" => [
        'class' => 'App\\Http\\Controllers\\StripeController',
        'method' => 'receivedPayment'
    ],

    /**
    * Webhook settings
    * class: Class name with namespace
    * method: Method name
    * This method will call after payment success/failed
    */

    "webhook" => [
        'class' => 'App\\Http\\Controllers\\StripeController',
        'method' => 'receivedWebhook'
    ],

    /**
    * Package settings
    * Info: Its your own local database table. Where are you storing your package information.
    * table: Table name
    * primary_key: Primary key of the table like id.
    * Uses: This package will use this table to get package information and store payment information and manage subscription.
    * Its very important to set this table and primary key.
    */

    'package' => [
        'table' => 'packages',
        'primary_key' => 'id',
    ],

    /**
     * Message settings
     * You can change the messages here
     */
    "message" => [
        "payment" => [
            "success" => [
                "title" => "Payment Done!",
                "message" => "Your payment has been successfully completed.",
            ],
            "failed" => [
                "title" => "Payment Failed!",
                "message" => "Your payment has been failed.",
            ],
        ],
        "subscription" => [
            "success" => [
                "title" => "Subscription Done!",
                "message" => "Your subscription has been successfully completed.",
            ],
            "failed" => [
                "title" => "Subscription Failed!",
                "message" => "Your subscription has been failed.",
            ],
        ],
    ]
];
