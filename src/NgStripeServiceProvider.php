<?php

namespace Nagorik\Stripe;
use Nagorik\Stripe\NgStripe;
use Illuminate\Support\ServiceProvider;
use Nagorik\Stripe\Commands\NgStripeInstallCommand;
use Nagorik\Stripe\Commands\NgStripeUninstallCommand;
use Nagorik\Stripe\Commands\NgStripeSubscriptionCheckup;

class NgStripeServiceProvider extends ServiceProvider
{

    protected $commands = [
        NgStripeInstallCommand::class,
        NgStripeUninstallCommand::class,
        NgStripeSubscriptionCheckup::class
    ];

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        // public config
        $this->publishes([
            __DIR__.'/Config/ngstripe.php' => config_path('ngstripe.php'),
            __DIR__.'/Assets' => public_path('ngstripe'),
            __DIR__.'/Views' => base_path('resources/views/vendor/ngstripe'),
        ], 'ngstripe');

        // public routes
        $this->loadRoutesFrom(__DIR__.'/Routes/ngstripe_route.php');

        // public views
        $this->loadViewsFrom(__DIR__.'/Views', 'ngstripe');

        // load migrations
        $this->loadMigrationsFrom(__DIR__.'/Models/migrations');
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $app = $this->app;

        // merge config
        $this->mergeConfigFrom(
            __DIR__.'/Config/ngstripe.php', 'ngstripe'
        );

        // register commands
        $this->commands($this->commands);

        $this->app->bind('ng-stripe', function ($app) {
            return new NgStripe($this->getNgStripeConfig($app));
        });
    }

    /**
     * Return ngstripe configuration as array
     *
     * @param  Application $app
     * @return array
     */
    private function getNgStripeConfig($app)
    {
        $config = $app['config']->get('ngstripe');

        if (is_null($config)) {
            return [];
        }

        return $config;
    }
}
