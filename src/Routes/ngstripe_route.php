<?php

use Illuminate\Support\Facades\Route;
use Nagorik\Stripe\Controllers\NgStripeController;

Route::get('/ngstripe/payment/dashboard', [NgStripeController::class, 'createPayment']);
Route::get('/ngstripe/payment/{status}', [NgStripeController::class, 'paymentStatus']);
Route::get('/ngstripe/download/invoice/{txn_id}', [NgStripeController::class, 'downloadInvoice']);
Route::any('/ngstripe/webhook', [NgStripeController::class, 'receieveWebhook']);
Route::get('/ngstripe/json', [NgStripeController::class, 'paymentJson']);
