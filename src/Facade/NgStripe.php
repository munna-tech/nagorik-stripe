<?php

namespace Nagorik\Stripe\Facade;

use Illuminate\Support\Facades\Facade;

class NgStripe extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ng-stripe';
    }
}
